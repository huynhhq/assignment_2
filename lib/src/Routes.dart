import 'package:flutter/material.dart';
import 'package:login_navigation/src/screens/login/index.dart';

class Routes {
  var routes = <String, WidgetBuilder>{
    "/": (BuildContext context) => LoginScreen()
  };

  Routes() {
    runApp(MaterialApp(
      theme: ThemeData(platform: TargetPlatform.iOS),
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      routes: routes,
    ));
  }
}
