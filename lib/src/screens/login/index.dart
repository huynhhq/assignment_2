import 'package:flutter/material.dart';
import 'package:login_navigation/src/helper/Constants.dart';
import 'package:login_navigation/src/helper/Helper.dart';
import 'dart:async';

import 'package:login_navigation/src/screens/home/index.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  Timer _timer;
  String errorMsg;
  bool isLoading;

  @override
  void initState() {
    isLoading = false;
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Stack(
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image:
                  new AssetImage("assets/images/background_login_screen.jpeg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        new Center(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              _showLogo(),
              _showEmailTextField(),
              _showPasswordTextField(),
              _showLoginButton(),
            ],
          ),
        )
      ],
    ));
  }

  Widget _showLogo() {
    return Center(
      child: Image.asset("assets/images/c_sharp_logo.png",
          width: 220, height: 110, fit: BoxFit.cover),
    );
  }

  Widget _showEmailTextField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 100.0, 30, 0.0),
      child: new TextFormField(
        style: TextStyle(fontSize: 20.0, color: Colors.white),
        controller: _emailController,
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        decoration: new InputDecoration(
            hintText: 'Email',
            hintStyle: TextStyle(fontSize: 20.0, color: Colors.white),
            icon: new Icon(
              Icons.account_circle,
              color: Colors.white,
            )),
      ),
    );
  }

  Widget _showPasswordTextField() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 15.0, 30, 0.0),
      child: new TextFormField(
        style: TextStyle(fontSize: 20.0, color: Colors.white),
        controller: _passwordController,
        maxLines: 1,
        obscureText: true,
        decoration: new InputDecoration(
            hintText: 'Password',
            hintStyle: TextStyle(fontSize: 20.0, color: Colors.white),
            icon: new Icon(
              Icons.lock,
              color: Colors.white,
            )),
      ),
    );
  }

  Widget _showLoginButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(100.0, 45.0, 100.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: _showContentButton(),
            onPressed: _validate,
          ),
        ));
  }

  void _validate() {
    setState(() {
      isLoading = true;
    });
    if (!checkValidForm()) {
      setState(() {
        isLoading = false;
      });
      _showErrorDialog();
    } else {
      _timer = new Timer(const Duration(milliseconds: 500), () {
        setState(() {
          isLoading = false;
        });
        var route = new MaterialPageRoute(
          builder: (BuildContext context) =>
              new HomeScreen(value: _emailController.text),
        );
        Navigator.of(context).push(route);
      });
    }
  }

  bool checkValidForm() {
    errorMsg = "";
    RegExp exp = new RegExp(
      regexCHeckEmail,
      caseSensitive: false,
      multiLine: false,
    );
    String value = _emailController.text;
    if (value == null || value.isEmpty) {
      errorMsg = kEmptyEmail;
      return false;
    }

    if (!exp.hasMatch(value.trim())) {
      errorMsg = kInvalidEmail;
      return false;
    }
    value = _passwordController.text;
    if (value == null || value.trim().length < 3) {
      errorMsg = kInvalidPassword;
      return false;
    }

    return true;
  }

  Widget _showContentButton() {
    if (!isLoading) {
      return Text('Login',
          style: new TextStyle(fontSize: 20.0, color: Colors.white));
    }
    return Center(
        child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white)));
  }

  Future<void> _showErrorDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Whoops!'),
          content: Text(errorMsg),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
